import { FC } from 'react';
import { Link } from 'react-router-dom';
import {
    Box,
    Card,
    CardContent,
    CardActions,
    Button,
    Typography,
} from '@mui/material';
import { useUser } from '../../../core/hooks/useUser';
import { ROUTES } from '../../../core/enums/ERoutes';
import './scss/LogoutComponent.scss';

const LogoutComponent: FC = () => {
    const { logout } = useUser();

    const handleClick = () => {
        logout();
    };

    return (
        <Box className="c-logout-component">
            <Card className="c-logout-component__card">
                <CardContent>
                    <Typography component="h1" variant="h5" align="center">
                        You are logged in
                    </Typography>
                    <Typography component="p" align="center">
                        Do you want to log out?
                    </Typography>
                </CardContent>
                <CardActions className="c-logout-component__actions">
                    <Button
                        variant="contained"
                        size="large"
                        onClick={handleClick}
                    >
                        Log out
                    </Button>
                    <Link
                        className="c-logout-component__link"
                        to={ROUTES.BASE_URL}
                    >
                        <Button variant="contained" size="large">
                            Back to Dashboard
                        </Button>
                    </Link>
                </CardActions>
            </Card>
        </Box>
    );
};

export default LogoutComponent;
