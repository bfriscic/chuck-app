import { FC } from 'react';
import { Alert } from '@mui/material';
import './scss/AlertComponent.scss';

type AlertColor = 'success' | 'info' | 'warning' | 'error';

interface IAlertProps {
    severity: AlertColor | undefined;
    message: string;
}

const AlertComponent: FC<IAlertProps> = (props) => {
    const { severity, message } = props;

    return (
        <Alert className="c-alert-component" severity={severity}>
            {message}
        </Alert>
    );
};

export default AlertComponent;
