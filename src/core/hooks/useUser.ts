import { useState, ChangeEvent } from 'react';
import { useNavigate } from 'react-router';
import { useAppSelector, useAppDispatch } from '../store/hooks';
import { api } from '../api';
import { setUser } from '../store/slices/userSlice';
import { setJokes, initialJokes } from '../store/slices/jokesSlice';
import { ROUTES } from '../enums/ERoutes';
import { MESSAGES } from '../enums/EMessages';
import { FORM_FIELDS } from '../enums/EFormFields';
import { IUser } from '../interfaces/IUser';
import { mapUserFromResponse } from '../api/models/user.model';

const useUser = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [error, setError] = useState<Array<string>>([]);
    const [hasError, setHasError] = useState<boolean>(false);
    const [success, setSuccess] = useState<string>('');
    const [isSuccess, setIsSuccess] = useState<boolean>(false);

    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const user = useAppSelector((state) => state.user);

    const isLoggedIn = (): boolean => user.uid !== '';

    const register = () => {
        api.firebase
            .registerWithEmailAndPassword(email, password)
            .then((res) => {
                const user = mapUserFromResponse(res);

                dispatch(setUser(user));
                setIsLoading(false);

                setSuccess(MESSAGES.FORM_SUCCESS);
                setIsSuccess(true);

                setTimeout(() => {
                    navigate(ROUTES.BASE_URL);
                }, 3000);
            })
            .catch((err) => {
                console.error(err);

                setHasError(true);
                setError((errs) => [...errs, MESSAGES.FORM_ERROR]);
                setIsLoading(false);
            });
    };

    const login = () => {
        api.firebase
            .logInWithEmailAndPassword(email, password)
            .then((res) => {
                const user = mapUserFromResponse(res);

                dispatch(setUser(user));
                setIsLoading(false);
                setIsSuccess(true);

                navigate(ROUTES.BASE_URL);
            })
            .catch((err) => {
                console.error(err);

                setHasError(true);
                setError((errs) => [...errs, MESSAGES.FORM_LOGIN_ERROR]);
                setIsLoading(false);
            });
    };

    const logout = () => {
        const loggedOutUser: IUser = {
            uid: '',
            email: '',
        };

        api.firebase.logout();

        dispatch(setUser(loggedOutUser));
        dispatch(setJokes(initialJokes));

        navigate(ROUTES.BASE_URL);
    };

    const startFormAction = () => {
        setIsLoading(true);

        setError([]);
        setHasError(false);

        setSuccess('');
        setIsSuccess(false);
    };

    const validateForm = () => {
        if (!email) {
            setIsLoading(false);
            setError((errs) => [...errs, MESSAGES.FORM_NO_EMAIL]);
            setHasError(true);
        }

        if (!password) {
            setIsLoading(false);
            setError((errs) => [...errs, MESSAGES.FORM_NO_PASSWORD]);
            setHasError(true);
        }

        if (password.length < 6) {
            setIsLoading(false);
            setError((errs) => [...errs, MESSAGES.FORM_SHORT_PASSWORD]);
            setHasError(true);
        }
    };

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;

        switch (name) {
            case FORM_FIELDS.EMAIL: {
                setEmail(value);
                break;
            }
            case FORM_FIELDS.PASSWORD: {
                setPassword(value);
                break;
            }
            default: {
                return;
            }
        }
    };

    return {
        user,
        isLoggedIn,
        register,
        login,
        logout,
        email,
        setEmail,
        password,
        setPassword,
        isLoading,
        setIsLoading,
        error,
        setError,
        hasError,
        setHasError,
        success,
        setSuccess,
        isSuccess,
        setIsSuccess,
        startFormAction,
        validateForm,
        handleInputChange,
    };
};

export { useUser };
