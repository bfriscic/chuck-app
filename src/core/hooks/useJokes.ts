import { useState, useContext, useCallback, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../store/hooks';
import { setJokes } from '../store/slices/jokesSlice';
import { FilterContext } from '../context/FilterContext';
import { api } from '../api';
import { mapJokeFromResponse } from '../api/models/joke.model';
import { MESSAGES } from '../enums/EMessages';
import { IJoke } from '../interfaces/IJoke';
import { IJokeResponse } from '../interfaces/IJokeResponse';

const useJokes = () => {
    const { filter } = useContext(FilterContext);

    const [filteredJokes, setFilteredJokes] = useState<Array<IJoke>>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const { baseUrl, searchUrl } = api.jokes;

    const dispatch = useAppDispatch();
    const jokes = useAppSelector((state) => state.jokes);

    const resetState = () => {
        setLoading(true);
        setError('');
    };

    const parseJokes = useCallback(
        (data: Array<IJokeResponse>, searchBy) => {
            const jokes = data.map((joke: IJokeResponse) =>
                mapJokeFromResponse(joke)
            );

            const payload = {
                data: jokes,
                count: data.length,
                searchTerm: searchBy,
            };

            dispatch(setJokes(payload));
            setLoading(false);
        },
        [dispatch]
    );

    const getAllJokes = useCallback(
        (controller) => {
            fetch(baseUrl, { signal: controller.signal })
                .then((response) => response.json())
                .then((data) => {
                    parseJokes(data.result, '');
                })
                .catch((err) => {
                    console.error(err);
                    setError(MESSAGES.FETCH_ERROR);
                });
        },
        [baseUrl, parseJokes]
    );

    const searchJokes = (searchTerm: string) => {
        fetch(`${searchUrl}${searchTerm}`)
            .then((response) => response.json())
            .then((data) => {
                parseJokes(data.result, searchTerm);
            })
            .catch((err) => {
                console.error(err);
                setError(MESSAGES.NO_JOKES_FOUND);
            });
    };

    useEffect(() => {
        const abortController = new AbortController();

        resetState();

        if (jokes.count > 0) {
            setLoading(false);
            return;
        }

        getAllJokes(abortController);

        return () => abortController.abort();
    }, [getAllJokes, dispatch, jokes.count]);

    useEffect(() => {
        const { data } = jokes;
        let tempFilteredJokes: Array<IJoke> = [];

        if (!loading && filter === '') setFilteredJokes(data);

        if (!loading) {
            tempFilteredJokes = data.filter((joke) =>
                joke.value.toLowerCase().includes(filter.toLowerCase())
            );

            if (tempFilteredJokes.length === 0) setFilteredJokes(data);

            setFilteredJokes(tempFilteredJokes);
        }
    }, [filter, loading, jokes]);

    return { jokes: filteredJokes, searchJokes, loading, error };
};

export { useJokes };
