import { FC } from 'react';
import { Routes, Route } from 'react-router';
import { ProtectedRoute } from '../ProtectedRoute';
import { ROUTES } from '../../enums/ERoutes';
import { HomePage } from '../../../modules/HomePage';
import { LoginPage } from '../../../modules/LoginPage';
import { RegisterPage } from '../../../modules/RegisterPage';
import { JokePage } from '../../../modules/JokePage';
import { ErrorPage } from '../../../modules/ErrorPage';

const RoutesProvider: FC = () => {
    return (
        <Routes>
            <Route
                path={ROUTES.BASE_URL}
                element={<ProtectedRoute childRoute={<HomePage />} />}
            />
            <Route path={ROUTES.LOGIN} element={<LoginPage />} />
            <Route path={ROUTES.REGISTER} element={<RegisterPage />} />
            <Route
                path={`${ROUTES.JOKE}/:id`}
                element={<ProtectedRoute childRoute={<JokePage />} />}
            />
            <Route path={ROUTES.DEFAULT} element={<ErrorPage />} />
        </Routes>
    );
};

export default RoutesProvider;
