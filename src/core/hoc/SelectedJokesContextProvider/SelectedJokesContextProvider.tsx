import { FC, useState } from 'react';
import { SelectedJokesContext } from '../../context/SelectedJokesContext';
import { IJoke } from '../../interfaces/IJoke';

const SelectedJokesContextProvider: FC = (props) => {
    const [selectedJokes, setSelectedJokes] = useState<Array<IJoke>>([]);
    const [showModal, setShowModal] = useState<boolean>(false);
    const { children } = props;

    const contextData = {
        selectedJokes,
        setSelectedJokes,
        showModal,
        setShowModal,
    };

    return (
        <SelectedJokesContext.Provider value={contextData}>
            {children}
        </SelectedJokesContext.Provider>
    );
};

export default SelectedJokesContextProvider;
