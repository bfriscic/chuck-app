import { FC } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Container } from '@mui/material';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { RoutesProvider } from '../RoutesProvider';
import './scss/Layout.scss';

const Layout: FC = () => {
    return (
        <Router>
            <Header />
            <main className="c-layout">
                <Container>
                    <RoutesProvider />
                </Container>
            </main>
            <Footer />
        </Router>
    );
};

export default Layout;
