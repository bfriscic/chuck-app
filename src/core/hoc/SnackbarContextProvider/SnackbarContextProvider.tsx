import { FC, useState } from 'react';
import { SnackbarContext } from '../../context/SnackbarContext';

const SnackbarContextProvider: FC = (props) => {
    const [showSnackbar, setShowSnackbar] = useState<boolean>(false);
    const [message, setMessage] = useState<string>('');
    const { children } = props;

    const contextData = {
        showSnackbar,
        setShowSnackbar,
        message,
        setMessage,
    };

    return (
        <SnackbarContext.Provider value={contextData}>
            {children}
        </SnackbarContext.Provider>
    );
};

export default SnackbarContextProvider;
