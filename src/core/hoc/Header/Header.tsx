import { FC } from 'react';
import { AppBar, Toolbar, Typography, Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';
import { ROUTES } from '../../enums/ERoutes';
import './scss/Header.scss';

const Header: FC = () => {
    const { user, isLoggedIn, logout } = useUser();

    const handleClick = () => {
        logout();
    };

    const renderUser = isLoggedIn() && `${user.email}@`;

    const renderLogo = (
        <Typography component="span" variant="h6">
            <Link to={ROUTES.BASE_URL} className="c-header__link">
                {renderUser}ChuckApp
            </Link>
        </Typography>
    );

    const renderUserActions = isLoggedIn() && (
        <Button
            color="warning"
            variant="outlined"
            className="c-header__button"
            onClick={handleClick}
        >
            Log out
        </Button>
    );

    return (
        <AppBar className="c-header" position="static">
            <Toolbar className="c-header__toolbar">
                {renderLogo}
                {renderUserActions}
            </Toolbar>
        </AppBar>
    );
};

export default Header;
