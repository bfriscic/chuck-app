import { FC } from 'react';
import { Link } from 'react-router-dom';
import { Container, List, ListItem } from '@mui/material';
import { ROUTES } from '../../enums/ERoutes';
import './scss/Footer.scss';

const Footer: FC = () => {
    const footerMenu = [
        { path: ROUTES.BASE_URL, label: 'Dashboard' },
        { path: ROUTES.LOGIN, label: 'Login' },
        { path: ROUTES.REGISTER, label: 'Register' },
    ];

    const renderFooterMenu = footerMenu.map((menuItem, index) => {
        const { path, label } = menuItem;
        return (
            <ListItem className="c-footer__item" key={index}>
                <Link className="c-footer__link" to={path}>
                    {label}
                </Link>
            </ListItem>
        );
    });

    return (
        <footer className="c-footer">
            <Container>
                <List className="c-footer__menu" sx={{ margin: '0 auto' }}>
                    {renderFooterMenu}
                </List>
            </Container>
        </footer>
    );
};

export default Footer;
