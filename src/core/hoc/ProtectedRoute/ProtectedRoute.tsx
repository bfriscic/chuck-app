import { FC, useEffect, ReactElement } from 'react';
import { useLocation, useNavigate } from 'react-router';
import { api } from '../../api';
import { useAppDispatch } from '../../store/hooks';
import { setUser } from '../../store/slices/userSlice';
import { Navigate } from 'react-router';
import { useUser } from '../../hooks/useUser';
import { ROUTES } from '../../enums/ERoutes';

interface IProtectedRouteProps {
    childRoute: ReactElement<any, any> | null;
}

const ProtectedRoute: FC<IProtectedRouteProps> = (props) => {
    const { childRoute } = props;
    const { isLoggedIn } = useUser();
    const dispatch = useAppDispatch();

    const prevRoute = useLocation();
    const navigate = useNavigate();

    const handleAuthStateChange = () => {
        return api.firebase.auth.onAuthStateChanged((user) => {
            if (user) {
                const { uid, email } = user;
                dispatch(setUser({ uid, email: (email || '').toString() }));
                navigate(prevRoute.pathname);
            }
        });
    };

    useEffect(() => {
        const unsubscribe = handleAuthStateChange();

        return () => {
            unsubscribe();
        };
    }, []);

    if (!isLoggedIn()) return <Navigate to={ROUTES.LOGIN} replace />;
    return childRoute;
};

export default ProtectedRoute;
