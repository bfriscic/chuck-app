import { FC, useState } from 'react';
import { FilterContext } from '../../context/FilterContext';

const FilterContextProvider: FC = (props) => {
    const [filter, setFilter] = useState<string>('');
    const { children } = props;

    const contextData = {
        filter,
        setFilter,
    };

    return (
        <FilterContext.Provider value={contextData}>
            {children}
        </FilterContext.Provider>
    );
};

export default FilterContextProvider;
