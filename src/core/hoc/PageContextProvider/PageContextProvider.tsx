import { FC, useState } from 'react';
import { PageContext } from '../../context/PageContext';
import { PAGE } from '../../enums/EPage';

const PageContextProvider: FC = (props) => {
    const [page, setPage] = useState<number>(PAGE.FIRST);
    const { children } = props;

    const contextData = {
        page,
        setPage,
    };

    return (
        <PageContext.Provider value={contextData}>
            {children}
        </PageContext.Provider>
    );
};

export default PageContextProvider;
