import { FC, useState } from 'react';
import { DrawerContext } from '../../context/DrawerContext';

const DrawerContextProvider: FC = (props) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const { children } = props;

    const contextData = {
        isOpen,
        setIsOpen,
    };

    return (
        <DrawerContext.Provider value={contextData}>
            {children}
        </DrawerContext.Provider>
    );
};

export default DrawerContextProvider;
