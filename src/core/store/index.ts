import { configureStore } from '@reduxjs/toolkit';
import userReducer from './slices/userSlice';
import jokesReducer from './slices/jokesSlice';

const store = configureStore({
    reducer: {
        user: userReducer,
        jokes: jokesReducer,
    },
});

export { store };
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
