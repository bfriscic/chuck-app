import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IJoke } from '../../interfaces/IJoke';
import { STORE } from '../../enums/EStore';

interface IJokesSlice {
    data: Array<IJoke>;
    count: number;
    searchTerm: string;
}

export const initialJokes: IJokesSlice = {
    data: [],
    count: 0,
    searchTerm: '',
};

const jokesSlice = createSlice({
    name: STORE.JOKES,
    initialState: initialJokes,
    reducers: {
        setJokes: (state, { payload }: PayloadAction<IJokesSlice>) => {
            state.data = payload.data;
            state.count = payload.count;
            state.searchTerm = payload.searchTerm;
        },
    },
});

export const { setJokes } = jokesSlice.actions;
export default jokesSlice.reducer;
