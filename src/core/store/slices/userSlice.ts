import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUser } from '../../interfaces/IUser';
import { STORE } from '../../enums/EStore';

const initialUser: IUser = {
    uid: '',
    email: '',
};

const userSlice = createSlice({
    name: STORE.USER,
    initialState: initialUser,
    reducers: {
        setUser: (state, { payload }: PayloadAction<IUser>) => {
            const { uid, email } = payload;

            state.uid = uid;
            state.email = email;
        },
        clearUser: (state) => {
            state.uid = '';
            state.email = '';
        },
    },
});

export const { setUser, clearUser } = userSlice.actions;
export default userSlice.reducer;
