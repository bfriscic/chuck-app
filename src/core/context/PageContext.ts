import { createContext } from 'react';
import { PAGE } from '../enums/EPage';

interface IPageContext {
    page: number;
    setPage: (n: number) => void;
}

const initialValues: IPageContext = {
    page: PAGE.FIRST,
    setPage: (n: number) => {},
};

const PageContext = createContext(initialValues);

export { PageContext };
