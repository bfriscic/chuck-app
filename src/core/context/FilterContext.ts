import { createContext } from 'react';

interface IFilterContext {
    filter: string;
    setFilter: (s: string) => void;
}

const initialValues: IFilterContext = {
    filter: '',
    setFilter: (s: string) => {},
};

const FilterContext = createContext(initialValues);

export { FilterContext };
