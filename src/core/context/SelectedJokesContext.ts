import { createContext } from 'react';
import { IJoke } from '../interfaces/IJoke';

interface ISelectedJokesContext {
    selectedJokes: Array<IJoke>;
    setSelectedJokes: (j: any) => void;
    showModal: boolean;
    setShowModal: (b: boolean) => void;
}

const initialValues: ISelectedJokesContext = {
    selectedJokes: [],
    setSelectedJokes: (j: Array<IJoke>) => {},
    showModal: false,
    setShowModal: (b: boolean) => {},
};

const SelectedJokesContext = createContext(initialValues);

export { SelectedJokesContext };
