import { createContext } from 'react';

interface IDrawerContext {
    isOpen: boolean;
    setIsOpen: (b: boolean) => void;
}

const initialValues: IDrawerContext = {
    isOpen: false,
    setIsOpen: (b: boolean) => {},
};

const DrawerContext = createContext(initialValues);

export { DrawerContext };
