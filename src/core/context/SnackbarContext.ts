import { createContext } from 'react';

interface ISnackbarContext {
    showSnackbar: boolean;
    setShowSnackbar: (b: boolean) => void;
    message: string;
    setMessage: (s: string) => void;
}

const initialValues: ISnackbarContext = {
    showSnackbar: false,
    setShowSnackbar: (b: boolean) => {},
    message: '',
    setMessage: (s: string) => {},
};

const SnackbarContext = createContext(initialValues);

export { SnackbarContext };
