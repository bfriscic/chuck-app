const baseSearchTerm = 'Chuck';
const baseUrl = `https://api.chucknorris.io/jokes/search?query=${baseSearchTerm}`;
const searchUrl = 'https://api.chucknorris.io/jokes/search?query=';

const jokes = {
    baseSearchTerm,
    baseUrl,
    searchUrl,
};

export { jokes };
