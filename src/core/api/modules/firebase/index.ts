import { initializeApp } from 'firebase/app';
import {
    getAuth,
    setPersistence,
    browserSessionPersistence,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    signOut,
} from 'firebase/auth';

const firebaseConfig = {
    apiKey: 'AIzaSyB_FaSnnah_gRF3DLLoPQxmJJ5smI_UCRA',
    authDomain: 'chuck-app-f8f6f.firebaseapp.com',
    databaseURL:
        'https://chuck-app-f8f6f-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'chuck-app-f8f6f',
    storageBucket: 'chuck-app-f8f6f.appspot.com',
    messagingSenderId: '883387542987',
    appId: '1:883387542987:web:ca7006cd69765de97b9a4e',
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const logInWithEmailAndPassword = async (email: string, password: string) => {
    try {
        await setPersistence(auth, browserSessionPersistence);

        const res = await signInWithEmailAndPassword(auth, email, password);
        const user = res.user;

        return user;
    } catch (err) {
        console.error(err);
    }
};

const registerWithEmailAndPassword = async (
    email: string,
    password: string
) => {
    try {
        await setPersistence(auth, browserSessionPersistence);

        const res = await createUserWithEmailAndPassword(auth, email, password);
        const user = res.user;

        return user;
    } catch (err) {
        console.error(err);
    }
};

const logout = () => {
    signOut(auth);
};

const firebase = {
    auth,
    logInWithEmailAndPassword,
    registerWithEmailAndPassword,
    logout,
};

export { firebase };
