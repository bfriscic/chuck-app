import { IUser } from '../../interfaces/IUser';

const mapUserFromResponse = (res: any): IUser => {
    const { uid, email } = res;

    return {
        uid,
        email,
    };
};

export { mapUserFromResponse };
