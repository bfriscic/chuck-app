import { IJokeResponse } from '../../interfaces/IJokeResponse';
import { IJoke } from '../../interfaces/IJoke';

const mapJokeFromResponse = (res: IJokeResponse): IJoke => {
    const { id, url, value, categories } = res;

    return {
        id,
        url,
        value,
        categories,
    };
};

export { mapJokeFromResponse };
