import { firebase } from './modules/firebase';
import { jokes } from './modules/jokes';

const api = {
    firebase,
    jokes,
};

export { api };
