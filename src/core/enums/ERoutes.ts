enum ROUTES {
    DEFAULT = '*',
    BASE_URL = '/',
    LOGIN = '/login',
    REGISTER = '/register',
    JOKE = 'joke',
}

export { ROUTES };
