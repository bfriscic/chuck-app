enum MESSAGES {
    FORM_ERROR = 'There was an error. Make sure the provided data is correct and try again.',
    FORM_SUCCESS = 'Success! Redirecting you to the Dashboard in 3 seconds.',
    FORM_NO_EMAIL = 'Please enter a valid email address',
    FORM_NO_PASSWORD = 'Please enter a password',
    FORM_SHORT_PASSWORD = 'Password must be at least 6 characters',
    FORM_LOGIN_ERROR = 'Incorrect user credentials, please try again',
    FETCH_ERROR = 'There was an error loading the joke archive. Please try again.',
    NO_JOKES_SELECTED = 'No jokes were selected - nothing to display.',
    NO_JOKES_FOUND = 'No Jokes were found for your term. Please try something else',
    SEARCH_RESET = 'Your search has been reset',
    SEARCH_RESULT = 'Showing search results for:',
    GENERIC_ERROR = 'There was an error... Please return to the dashboard and try again.',
}

export { MESSAGES };
