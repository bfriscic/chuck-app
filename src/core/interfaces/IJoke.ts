export interface IJoke {
    id: string;
    url: string;
    value: string;
    categories: Array<string>;
}
