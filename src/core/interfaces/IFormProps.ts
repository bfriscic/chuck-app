import { FormEvent, ChangeEvent } from 'react';

export interface IFormProps {
    handleSubmit: (event: FormEvent<HTMLFormElement>) => void;
    handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void;
}
