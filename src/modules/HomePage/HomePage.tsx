import { FC, useEffect } from 'react';
import { Container } from '@mui/material';
import { HomeContextProvider } from './components/HomeContextProvider';
import { HomeHeading } from './components/HomeHeading';
import { JokesTable } from './components/JokesTable';
import { SelectedJokesModal } from './components/SelectedJokesModal';
import { FiltersDrawer } from './components/FiltersDrawer';
import { SnackbarNotification } from './components/SnackbarNotification';
import { scrollToTop } from '../../shared/utils/scrollToTop';
import './scss/HomePage.scss';

const HomePage: FC = () => {
    useEffect(() => {
        scrollToTop();
    }, []);

    const renderContent = (
        <HomeContextProvider>
            <HomeHeading />
            <SnackbarNotification />
            <JokesTable />
            <SelectedJokesModal />
            <FiltersDrawer />
        </HomeContextProvider>
    );

    return (
        <section className="c-home-page">
            <Container>{renderContent}</Container>
        </section>
    );
};

export default HomePage;
