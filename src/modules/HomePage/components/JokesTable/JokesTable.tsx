import {
    FC,
    useState,
    useContext,
    useMemo,
    useCallback,
    ChangeEvent,
} from 'react';
import { Link } from 'react-router-dom';
import {
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TablePagination,
    TableRow,
    Paper,
    Button,
    Checkbox,
} from '@mui/material';
import { useJokes } from '../../../../core/hooks/useJokes';
import { ControlsGrid } from '../ControlsGrid';
import { LoadingBar } from '../../../../shared/components/LoadingBar';
import { AlertComponent } from '../../../../shared/components/AlertComponent';
import { SelectedJokesContext } from '../../../../core/context/SelectedJokesContext';
import { PageContext } from '../../../../core/context/PageContext';
import { ROUTES } from '../../../../core/enums/ERoutes';
import { TPagination } from '../../../../core/types/TPagination';
import { IJoke } from '../../../../core/interfaces/IJoke';
import { PAGE } from '../../../../core/enums/EPage';

const JokesTable: FC = () => {
    const { jokes, loading, error } = useJokes();

    const { selectedJokes, setSelectedJokes } =
        useContext(SelectedJokesContext);
    const { page, setPage } = useContext(PageContext);

    const [rowsPerPage, setRowsPerPage] = useState<TPagination>(10);

    const totalPages = useMemo(
        () => Math.ceil(jokes.length / rowsPerPage),
        [jokes.length, rowsPerPage]
    );

    const calculateIsChecked = useCallback(
        (id) =>
            !!selectedJokes.find((selectedJoke) => selectedJoke.id === id)
                ? true
                : false,
        [selectedJokes]
    );

    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - jokes.length) : 0;

    const handleChangePage = (event: any, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10) as TPagination);
        setPage(PAGE.FIRST);
    };

    const handleSelectJoke = (
        event: ChangeEvent<HTMLInputElement>,
        checked: boolean
    ) => {
        const { value: id, checked: isChecked } = event.target;
        const selectedJoke = jokes.find((joke) => joke.id === id);

        if (isChecked) {
            setSelectedJokes((j: Array<IJoke>) => [...j, selectedJoke]);
        } else {
            setSelectedJokes((j: Array<IJoke>) =>
                j.filter((joke) => joke.id !== id)
            );
        }
    };

    const renderLoading = loading && !error && <LoadingBar />;

    const renderTableHead = (
        <TableHead>
            <TableRow>
                <TableCell>Selected</TableCell>
                <TableCell>ID</TableCell>
                <TableCell>Joke</TableCell>
                <TableCell>Link</TableCell>
            </TableRow>
        </TableHead>
    );

    const renderTableBody = (
        <TableBody>
            {(rowsPerPage > 0
                ? jokes.slice(
                      page * rowsPerPage,
                      page * rowsPerPage + rowsPerPage
                  )
                : jokes
            ).map((joke, index) => (
                <TableRow key={index}>
                    <TableCell>
                        <Checkbox
                            onChange={handleSelectJoke}
                            value={joke.id}
                            checked={calculateIsChecked(joke.id)}
                        />
                    </TableCell>
                    <TableCell component="th" scope="row">
                        {joke.id}
                    </TableCell>
                    <TableCell component="th" scope="row">
                        {joke.value}
                    </TableCell>
                    <TableCell component="th" scope="row">
                        <Link
                            className="c-home-page__link"
                            to={`${ROUTES.JOKE}/${joke.id}`}
                        >
                            <Button variant="outlined">Details</Button>
                        </Link>
                    </TableCell>
                </TableRow>
            ))}

            {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                </TableRow>
            )}
        </TableBody>
    );

    const renderTableFooter = (
        <TableFooter>
            <TableRow>
                <TablePagination
                    rowsPerPageOptions={[10, 20, 50, 100, 200]}
                    count={jokes.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={(event, newPage) =>
                        handleChangePage(event, newPage)
                    }
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelDisplayedRows={({ page }) => {
                        return `Page: ${page + 1}/${totalPages}`;
                    }}
                    showFirstButton={true}
                    showLastButton={true}
                    labelRowsPerPage={<span>Rows:</span>}
                />
            </TableRow>
        </TableFooter>
    );

    const renderTable = !loading && (
        <>
            <ControlsGrid />
            <TableContainer component={Paper}>
                <Table>
                    {renderTableHead}
                    {renderTableBody}
                    {renderTableFooter}
                </Table>
            </TableContainer>
        </>
    );

    const renderError = error && (
        <AlertComponent severity="error" message={error} />
    );

    return (
        <>
            {renderLoading}
            {renderTable}
            {renderError}
        </>
    );
};

export default JokesTable;
