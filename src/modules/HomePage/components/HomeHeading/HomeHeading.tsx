import { FC, useMemo } from 'react';
import { Typography } from '@mui/material';
import { api } from '../../../../core/api';
import { useAppSelector } from '../../../../core/store/hooks';

const HomeHeading: FC = () => {
    const searchTerm = useAppSelector((state) => state.jokes.searchTerm);

    const description = useMemo(
        () =>
            searchTerm === '' || searchTerm === api.jokes.baseSearchTerm
                ? 'All Jokes'
                : `Search: ${searchTerm}`,
        [searchTerm]
    );

    return (
        <Typography component="h1" variant="h4" mb={5}>
            Chuck Norris Jokes Archive | {description}
        </Typography>
    );
};

export default HomeHeading;
