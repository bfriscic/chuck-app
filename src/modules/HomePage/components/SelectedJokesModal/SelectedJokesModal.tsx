import { FC, useContext } from 'react';
import {
    Modal,
    Container,
    Paper,
    Typography,
    Grid,
    Box,
    Button,
} from '@mui/material';
import { SelectedJokesContext } from '../../../../core/context/SelectedJokesContext';
import { JokeCard } from './components/JokeCard';
import { AlertComponent } from '../../../../shared/components/AlertComponent';
import { MESSAGES } from '../../../../core/enums/EMessages';
import './scss/SelectedJokesModal.scss';

const SelectedJokesModal: FC = () => {
    const { selectedJokes, showModal, setShowModal } =
        useContext(SelectedJokesContext);

    const handleClose = () => {
        setShowModal(false);
    };

    const renderHeading = (
        <Typography
            className="c-selected-jokes-modal__heading"
            component="h2"
            variant="h5"
        >
            Your Selected Jokes
        </Typography>
    );

    const renderError = selectedJokes.length === 0 && (
        <AlertComponent severity="error" message={MESSAGES.NO_JOKES_SELECTED} />
    );

    const renderSelectedJokes = selectedJokes.length > 0 && (
        <Grid container spacing={3}>
            {selectedJokes.map((joke, index) => (
                <Grid key={index} item xs={6}>
                    <JokeCard joke={joke} />
                </Grid>
            ))}
        </Grid>
    );

    const renderBody = (
        <Box className="c-selected-jokes-modal__content">
            {renderError}
            {renderSelectedJokes}
        </Box>
    );

    const renderFooter = (
        <Box className="c-selected-jokes-modal__footer">
            <Button variant="contained" onClick={handleClose}>
                Close Modal
            </Button>
        </Box>
    );

    return (
        <Modal
            className="c-selected-jokes-modal"
            open={showModal}
            onClose={handleClose}
        >
            <Container>
                <Paper className="c-selected-jokes-modal__body">
                    {renderHeading}
                    {renderBody}
                    {renderFooter}
                </Paper>
            </Container>
        </Modal>
    );
};

export default SelectedJokesModal;
