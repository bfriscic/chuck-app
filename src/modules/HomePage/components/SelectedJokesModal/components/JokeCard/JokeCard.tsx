import { FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    Card,
    CardContent,
    CardActions,
    Typography,
    Box,
    Badge,
    Button,
    Link,
} from '@mui/material';
import { ROUTES } from '../../../../../../core/enums/ERoutes';
import { IJoke } from '../../../../../../core/interfaces/IJoke';
import './scss/JokeCard.scss';

interface IJokeCardProps {
    joke: IJoke;
}

const JokeCard: FC<IJokeCardProps> = (props) => {
    const { id, value, url, categories } = props.joke;

    const renderJoke = (
        <Typography className="c-joke-card__joke" component="h3" variant="h6">
            {value}
        </Typography>
    );

    const renderId = <Typography component="p">ID: {id}</Typography>;

    const renderCategories = categories.length > 0 && (
        <Box>
            {categories.map((category, index) => (
                <Badge key={index} badgeContent={category} color="primary" />
            ))}
        </Box>
    );

    const renderContent = (
        <CardContent>
            {renderJoke}
            {renderId}
            {renderCategories}
        </CardContent>
    );

    const renderActions = (
        <CardActions>
            <RouterLink
                className="c-joke-card__link"
                to={`${ROUTES.JOKE}/${id}`}
            >
                <Button variant="outlined">Details</Button>
            </RouterLink>

            <Link className="c-joke-card__link" href={url} target="_blank">
                <Button variant="outlined">Go to Joke Page</Button>
            </Link>
        </CardActions>
    );

    return (
        <Card className="c-joke-card">
            {renderContent}
            {renderActions}
        </Card>
    );
};

export default JokeCard;
