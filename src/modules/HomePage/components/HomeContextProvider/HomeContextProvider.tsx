import { FC } from 'react';
import { DrawerContextProvider } from '../../../../core/hoc/DrawerContexProvider';
import { FilterContextProvider } from '../../../../core/hoc/FilterContextProvider';
import { PageContextProvider } from '../../../../core/hoc/PageContextProvider';
import { SelectedJokesContextProvider } from '../../../../core/hoc/SelectedJokesContextProvider';
import { SnackbarContextProvider } from '../../../../core/hoc/SnackbarContextProvider';

const HomeContextProvider: FC = (props) => {
    const { children } = props;

    return (
        <SelectedJokesContextProvider>
            <DrawerContextProvider>
                <FilterContextProvider>
                    <PageContextProvider>
                        <SnackbarContextProvider>
                            {children}
                        </SnackbarContextProvider>
                    </PageContextProvider>
                </FilterContextProvider>
            </DrawerContextProvider>
        </SelectedJokesContextProvider>
    );
};

export default HomeContextProvider;
