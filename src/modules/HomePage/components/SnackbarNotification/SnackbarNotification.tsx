import { FC, useContext } from 'react';
import { Snackbar, IconButton } from '@mui/material';
import { SnackbarContext } from '../../../../core/context/SnackbarContext';

const SnackbarNotification: FC = () => {
    const { showSnackbar, setShowSnackbar, message } =
        useContext(SnackbarContext);

    const handleClose = () => {
        setShowSnackbar(false);
    };

    const snackbarAction = (
        <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
        >
            &times;
        </IconButton>
    );

    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={showSnackbar}
            onClose={handleClose}
            message={message}
            action={snackbarAction}
            autoHideDuration={3000}
        />
    );
};

export default SnackbarNotification;
