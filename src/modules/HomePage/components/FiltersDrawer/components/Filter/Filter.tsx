import { FC, useContext, useMemo, ChangeEvent } from 'react';
import { TextField, Button, Box } from '@mui/material';
import { FilterContext } from '../../../../../../core/context/FilterContext';
import { PageContext } from '../../../../../../core/context/PageContext';
import { PAGE } from '../../../../../../core/enums/EPage';

const Filter: FC = () => {
    const { filter, setFilter } = useContext(FilterContext);
    const { setPage } = useContext(PageContext);

    const handleChange = (
        event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        setPage(PAGE.FIRST);
        setFilter(event.target.value);
    };

    const handleClick = () => {
        setPage(PAGE.FIRST);
        setFilter('');
    };

    const isClearDisabled = useMemo(
        () => (filter === '' ? true : false),
        [filter]
    );

    return (
        <Box>
            <TextField
                label="Filter Jokes"
                variant="outlined"
                sx={{ width: 1 }}
                value={filter}
                onChange={handleChange}
                InputProps={{
                    endAdornment: (
                        <Button
                            variant="contained"
                            onClick={handleClick}
                            disabled={isClearDisabled}
                        >
                            X
                        </Button>
                    ),
                }}
            />
        </Box>
    );
};

export { Filter };
