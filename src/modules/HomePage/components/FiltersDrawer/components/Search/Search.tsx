import {
    FC,
    useState,
    useMemo,
    useContext,
    ChangeEvent,
    FormEvent,
    MouseEvent,
} from 'react';
import { TextField, Button, Box, Grid, Tooltip } from '@mui/material';
import { api } from '../../../../../../core/api';
import { useJokes } from '../../../../../../core/hooks/useJokes';
import { DrawerContext } from '../../../../../../core/context/DrawerContext';
import { SnackbarContext } from '../../../../../../core/context/SnackbarContext';
import { FilterContext } from '../../../../../../core/context/FilterContext';
import { SelectedJokesContext } from '../../../../../../core/context/SelectedJokesContext';
import { MESSAGES } from '../../../../../../core/enums/EMessages';
import './scss/Search.scss';

const Search: FC = () => {
    const { searchJokes } = useJokes();

    const { setIsOpen } = useContext(DrawerContext);
    const { setShowSnackbar, setMessage } = useContext(SnackbarContext);
    const { setFilter } = useContext(FilterContext);
    const { setSelectedJokes } = useContext(SelectedJokesContext);

    const [searchTerm, setSearchTerm] = useState<string>('');

    const isSearchDisabled = useMemo(
        () => (searchTerm === '' ? true : false),
        [searchTerm]
    );

    const setUI = (message: string) => {
        setFilter('');
        setSearchTerm('');
        setSelectedJokes([]);
        setIsOpen(false);
        setShowSnackbar(true);
        setMessage(message);
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        searchJokes(searchTerm);
        setUI(`${MESSAGES.SEARCH_RESULT} "${searchTerm}"`);
    };

    const handleReset = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        searchJokes(api.jokes.baseSearchTerm);
        setUI(MESSAGES.SEARCH_RESET);
    };

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setSearchTerm(value);
    };

    return (
        <Box
            className="c-search"
            component="form"
            onSubmit={handleSubmit}
            noValidate
        >
            <Grid container>
                <Grid item xs={8}>
                    <TextField
                        label="Search Jokes"
                        variant="outlined"
                        sx={{ width: 1 }}
                        value={searchTerm}
                        onChange={handleChange}
                    />
                </Grid>
                <Grid className="c-search__button-container" item xs={2}>
                    <Button
                        className="c-search__button"
                        type="submit"
                        variant="contained"
                        disabled={isSearchDisabled}
                    >
                        Search Jokes
                    </Button>
                </Grid>
                <Grid className="c-search__button-container" item xs={2}>
                    <Tooltip
                        title="This action will reset the jokes, filter and selected jokes"
                        placement="top-start"
                    >
                        <Button
                            className="c-search__button"
                            type="submit"
                            variant="contained"
                            onClick={handleReset}
                        >
                            Reset Search
                        </Button>
                    </Tooltip>
                </Grid>
            </Grid>
        </Box>
    );
};

export default Search;
