import { FC, useContext } from 'react';
import {
    Drawer,
    Box,
    Container,
    Grid,
    Typography,
    Button,
} from '@mui/material';
import { DrawerContext } from '../../../../core/context/DrawerContext';
import { Filter } from './components/Filter';
import { Search } from './components/Search';
import './scss/FiltersDrawer.scss';

const FiltersDrawer: FC = () => {
    const { isOpen, setIsOpen } = useContext(DrawerContext);

    const handleClose = () => {
        setIsOpen(false);
    };

    return (
        <Drawer anchor="bottom" open={isOpen} onClose={handleClose}>
            <Box className="c-filters-drawer">
                <Container>
                    <Grid container rowSpacing={5} columnSpacing={0}>
                        <Grid item xs={8}>
                            <Typography component="h2" variant="h5">
                                Search & Filter Jokes
                            </Typography>
                        </Grid>
                        <Grid
                            className="c-filters-drawer__item c-filters-drawer__item--right"
                            item
                            xs={4}
                        >
                            <Button variant="contained" onClick={handleClose}>
                                Close
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Filter />
                        </Grid>
                        <Grid item xs={12}>
                            <Search />
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </Drawer>
    );
};

export default FiltersDrawer;
