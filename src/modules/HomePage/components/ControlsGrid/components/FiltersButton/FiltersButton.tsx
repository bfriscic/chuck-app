import { FC, useContext } from 'react';
import { Button } from '@mui/material';
import { DrawerContext } from '../../../../../../core/context/DrawerContext';

const FiltersButton: FC = () => {
    const { setIsOpen } = useContext(DrawerContext);

    const handleClick = () => {
        setIsOpen(true);
    };

    return (
        <Button
            className="c-reset-button"
            variant="contained"
            onClick={handleClick}
        >
            Filters
        </Button>
    );
};

export default FiltersButton;
