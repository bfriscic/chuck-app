import { FC, useContext } from 'react';
import { Button } from '@mui/material';
import { SelectedJokesContext } from '../../../../../../core/context/SelectedJokesContext';
import './scss/DetailsButton.scss';

const ShowSelectedButton: FC = () => {
    const { setShowModal } = useContext(SelectedJokesContext);

    const handleClick = () => {
        setShowModal(true);
    };

    return (
        <Button
            className="c-details-button"
            variant="contained"
            onClick={() => handleClick()}
        >
            Show Selected
        </Button>
    );
};

export default ShowSelectedButton;
