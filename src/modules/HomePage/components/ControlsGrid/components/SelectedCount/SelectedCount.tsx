import { FC, useState, useEffect, useContext } from 'react';
import { Typography } from '@mui/material';
import { SelectedJokesContext } from '../../../../../../core/context/SelectedJokesContext';

const SelectedCount: FC = () => {
    const { selectedJokes } = useContext(SelectedJokesContext);
    const [selectedCount, setSelectedCount] = useState<number>(0);

    useEffect(() => {
        setSelectedCount(selectedJokes.length);
    }, [selectedJokes]);

    const renderCount = selectedCount > 0 && (
        <Typography component="span" variant="h6">
            You have currently selected {selectedCount} joke
            {selectedCount > 1 ? 's' : ''}
        </Typography>
    );

    const renderNoJokesSelected = selectedCount === 0 && (
        <Typography component="span" variant="h6">
            You haven't currently selected any jokes.
        </Typography>
    );

    return (
        <>
            {renderCount}
            {renderNoJokesSelected}
        </>
    );
};

export default SelectedCount;
