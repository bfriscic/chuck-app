import { FC, useState, useContext, useEffect } from 'react';
import { Button } from '@mui/material';
import { SelectedJokesContext } from '../../../../../../core/context/SelectedJokesContext';
import './scss/ResetButton.scss';

const ResetButton: FC = () => {
    const { selectedJokes, setSelectedJokes } =
        useContext(SelectedJokesContext);
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(true);

    useEffect(() => {
        const shouldBeDisabled = selectedJokes.length === 0 ? true : false;
        setButtonDisabled(shouldBeDisabled);
    }, [selectedJokes]);

    const handleClick = () => {
        setSelectedJokes([]);
    };

    return (
        <Button
            className="c-reset-button"
            variant="contained"
            onClick={handleClick}
            disabled={buttonDisabled}
        >
            Reset
        </Button>
    );
};

export default ResetButton;
