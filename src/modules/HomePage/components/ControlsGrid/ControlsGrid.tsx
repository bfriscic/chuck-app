import { FC } from 'react';
import { Paper, Grid } from '@mui/material';
import { SelectedCount } from './components/SelectedCount';
import { ResetButton } from './components/ResetButton';
import { ShowSelectedButton } from './components/SnowSelectedButton';
import { FiltersButton } from './components/FiltersButton';
import './scss/ControlsGrid.scss';

const ControlsGrid: FC = () => {
    return (
        <Paper className="c-controls-grid">
            <Grid container rowSpacing={2} columnSpacing={3}>
                <Grid item xs={6}>
                    <SelectedCount />
                </Grid>
                <Grid item xs={2}>
                    <ShowSelectedButton />
                </Grid>
                <Grid item xs={2}>
                    <ResetButton />
                </Grid>
                <Grid item xs={2}>
                    <FiltersButton />
                </Grid>
            </Grid>
        </Paper>
    );
};

export default ControlsGrid;
