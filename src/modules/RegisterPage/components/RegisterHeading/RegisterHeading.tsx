import { FC } from 'react';
import { Typography } from '@mui/material';

const RegisterHeading: FC = () => {
    return (
        <>
            <Typography component="h1" variant="h5" align="center">
                Don't Have an Account Yet?
            </Typography>
            <Typography component="h2" variant="h5" align="center">
                Register!
            </Typography>
        </>
    );
};

export default RegisterHeading;
