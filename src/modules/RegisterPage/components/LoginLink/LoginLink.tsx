import { FC } from 'react';
import { Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import { ROUTES } from '../../../../core/enums/ERoutes';

const LoginLink: FC = () => {
    return (
        <>
            <hr />
            <Typography component="p" align="center">
                Already have an account? Log in{' '}
                <Link className="c-register-page__link" to={ROUTES.LOGIN}>
                    here
                </Link>
                .
            </Typography>
        </>
    );
};

export default LoginLink;
