import { FC } from 'react';
import { TextField, Button, Box } from '@mui/material';
import { FORM_FIELDS } from '../../../../core/enums/EFormFields';
import { IFormProps } from '../../../../core/interfaces/IFormProps';

const RegisterForm: FC<IFormProps> = (props) => {
    const { handleSubmit, handleInputChange } = props;

    return (
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
                margin="normal"
                required
                fullWidth
                id={FORM_FIELDS.EMAIL}
                label="Email Address"
                name={FORM_FIELDS.EMAIL}
                autoComplete="email"
                autoFocus
                onChange={handleInputChange}
            />
            <TextField
                margin="normal"
                required
                fullWidth
                id={FORM_FIELDS.PASSWORD}
                name={FORM_FIELDS.PASSWORD}
                label="Password"
                type="password"
                onChange={handleInputChange}
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
            >
                Register
            </Button>
        </Box>
    );
};

export default RegisterForm;
