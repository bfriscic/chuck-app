import { FC, useEffect, FormEvent } from 'react';
import { useUser } from '../../core/hooks/useUser';
import { AlertComponent } from '../../shared/components/AlertComponent';
import { LoadingBar } from '../../shared/components/LoadingBar';
import { LogoutComponent } from '../../shared/components/LogoutComponent';
import { RegisterHeading } from './components/RegisterHeading';
import { RegisterForm } from './components/RegisterForm';
import { LoginLink } from './components/LoginLink';
import { scrollToTop } from '../../shared/utils/scrollToTop';
import './scss/RegisterPage.scss';

const RegisterPage: FC = () => {
    const {
        isLoggedIn,
        register,
        isLoading,
        error,
        hasError,
        success,
        isSuccess,
        startFormAction,
        validateForm,
        handleInputChange,
    } = useUser();

    useEffect(() => {
        scrollToTop();
    }, []);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        startFormAction();
        validateForm();

        register();
    };

    const renderHeading = !isLoading && !isSuccess && <RegisterHeading />;

    const renderForm = !isLoading && !isSuccess && (
        <>
            <RegisterForm
                handleSubmit={handleSubmit}
                handleInputChange={handleInputChange}
            />
            <LoginLink />
        </>
    );

    const renderLoading = isLoading && <LoadingBar />;

    const renderError =
        hasError &&
        error &&
        !isLoading &&
        error.map((err, index) => (
            <AlertComponent key={index} severity="error" message={err} />
        ));

    const renderRegisterScreen = !isLoggedIn() && (
        <>
            {renderHeading}
            {renderForm}
            {renderLoading}
            {renderError}
        </>
    );

    const renderSuccess = isSuccess &&
        success &&
        !isLoading &&
        isLoggedIn() && <AlertComponent severity="success" message={success} />;

    const renderLogoutScreen = !isSuccess && isLoggedIn() && (
        <LogoutComponent />
    );

    return (
        <section className="o-page c-register-page">
            {renderRegisterScreen}
            {renderSuccess}
            {renderLogoutScreen}
        </section>
    );
};

export default RegisterPage;
