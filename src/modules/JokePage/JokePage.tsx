import { FC, useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import { useAppSelector } from '../../core/store/hooks';
import {
    Card,
    CardActions,
    CardContent,
    Button,
    Link,
    Typography,
    Badge,
    Box,
} from '@mui/material';
import { useJokes } from '../../core/hooks/useJokes';
import { LoadingBar } from '../../shared/components/LoadingBar';
import { scrollToTop } from '../../shared/utils/scrollToTop';
import { ROUTES } from '../../core/enums/ERoutes';
import { IJoke } from '../../core/interfaces/IJoke';
import './scss/JokePage.scss';

const JokePage: FC = () => {
    const { id } = useParams();
    let jokes = useAppSelector((state) => state.jokes.data);

    const { jokes: apiJokes } = useJokes();

    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [joke, setJoke] = useState<IJoke | null>(null);

    const getJokeData = () => {
        const currentJoke = jokes.find((j) => j.id === id);

        if (currentJoke) setJoke(currentJoke);
        setIsLoading(false);
    };

    useEffect(() => {
        setIsLoading(true);

        getJokeData();

        if (jokes.length !== 0) jokes = apiJokes;
    }, [id, jokes, apiJokes]);

    useEffect(() => {
        scrollToTop();
    }, []);

    const renderLoading = (isLoading || !joke) && <LoadingBar />;

    const renderCategories = joke && (
        <Box sx={{ padding: '0.75rem 2.25rem' }}>
            {joke.categories.map((category, index) => (
                <Badge key={index} badgeContent={category} color="primary" />
            ))}
        </Box>
    );

    const renderContent = !isLoading && joke && (
        <>
            <CardContent>
                <Typography component="h1" variant="h4">
                    {joke.value}
                </Typography>
                <Typography component="p" variant="h6">
                    ID: {joke.id}
                </Typography>
                {renderCategories}
            </CardContent>
            <CardActions>
                <RouterLink className="c-joke-page__link" to={ROUTES.BASE_URL}>
                    <Button variant="outlined">Back</Button>
                </RouterLink>

                <Link
                    className="c-joke-page__link"
                    href={joke.url}
                    target="_blank"
                >
                    <Button variant="outlined">Go to Joke Page</Button>
                </Link>
            </CardActions>
        </>
    );

    return (
        <section className="c-joke-page o-page">
            <Card className="c-joke-page__card">
                {renderLoading}
                {renderContent}
            </Card>
        </section>
    );
};

export default JokePage;
