import { FC, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Typography, Container, Grid, Button } from '@mui/material';
import { AlertComponent } from '../../shared/components/AlertComponent';
import { scrollToTop } from '../../shared/utils/scrollToTop';
import { MESSAGES } from '../../core/enums/EMessages';
import { ROUTES } from '../../core/enums/ERoutes';
import './scss/ErrorPage.scss';

const ErrorPage: FC = () => {
    useEffect(() => {
        scrollToTop();
    }, []);

    return (
        <section className="c-error-page">
            <Container>
                <Grid container rowGap={3}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h4">
                            Something went wrong...
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <AlertComponent
                            severity="error"
                            message={MESSAGES.GENERIC_ERROR}
                        />
                    </Grid>
                    <Grid className="c-error-page__link-container" item xs={12}>
                        <Link
                            className="c-error-page__link"
                            to={ROUTES.BASE_URL}
                        >
                            <Button variant="outlined">
                                Back to Dashboard
                            </Button>
                        </Link>
                    </Grid>
                </Grid>
            </Container>
        </section>
    );
};

export default ErrorPage;
