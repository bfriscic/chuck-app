import { FC, useEffect, FormEvent } from 'react';
import { useUser } from '../../core/hooks/useUser';
import { LoginHeading } from './components/LoginHeading';
import { LoginForm } from './components/LoginForm';
import { RegisterLink } from './components/RegisterLink';
import { AlertComponent } from '../../shared/components/AlertComponent';
import { LogoutComponent } from '../../shared/components/LogoutComponent';
import { LoadingBar } from '../../shared/components/LoadingBar';
import { scrollToTop } from '../../shared/utils/scrollToTop';
import './scss/LoginPage.scss';

const LoginPage: FC = () => {
    const {
        isLoggedIn,
        login,
        error,
        hasError,
        isLoading,
        isSuccess,
        startFormAction,
        handleInputChange,
    } = useUser();

    useEffect(() => {
        scrollToTop();
    }, []);

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        startFormAction();

        login();
    };

    const renderHeading = !isLoading && !isSuccess && <LoginHeading />;

    const renderForm = !isLoading && !isSuccess && (
        <>
            <LoginForm
                handleSubmit={handleSubmit}
                handleInputChange={handleInputChange}
            />
            <RegisterLink />
        </>
    );

    const renderLoading = isLoading && <LoadingBar />;

    const renderError =
        hasError &&
        error &&
        !isLoading &&
        error.map((err, index) => (
            <AlertComponent key={index} severity="error" message={err} />
        ));

    const renderLoginScreen = !isLoggedIn() && (
        <>
            {renderHeading}
            {renderForm}
            {renderLoading}
            {renderError}
        </>
    );

    const renderLogoutScreen = !isSuccess && isLoggedIn() && (
        <LogoutComponent />
    );

    return (
        <section className="o-page c-login-page">
            {renderLoginScreen}
            {renderLogoutScreen}
        </section>
    );
};

export default LoginPage;
