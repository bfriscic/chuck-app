import { FC } from 'react';
import { Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import { ROUTES } from '../../../../core/enums/ERoutes';

const RegisterLink: FC = () => {
    return (
        <>
            <hr />
            <Typography component="p" align="center">
                Don't have an account? Register{' '}
                <Link className="c-register-page__link" to={ROUTES.REGISTER}>
                    here
                </Link>
                .
            </Typography>
        </>
    );
};

export default RegisterLink;
