import { FC } from 'react';
import { Typography } from '@mui/material';

const LoginHeading: FC = () => {
    return (
        <Typography component="h1" variant="h5" align="center">
            Log Into Your Account
        </Typography>
    );
};

export default LoginHeading;
