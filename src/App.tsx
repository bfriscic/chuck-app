import { FC } from 'react';
import { StoreProvider } from './core/hoc/StoreProvider';

const App: FC = () => {
    return (
        <div className="app">
            <StoreProvider />
        </div>
    );
};

export { App };
